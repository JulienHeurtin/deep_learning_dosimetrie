# Deep Learning Dosimetrie 
# Explication des Dossiers et fichiers

## Echantillon test 

Dossier comprenant les tableaux numpy où sont stockés les liens vers les patients de l'échantillon de test. 
Données stockées dans un fichier numpy pour y accéder à partir d'un autre notebooks facilement. 
Deux fichiers, échantillon de test avec thorax et échantillon avec thorax et pancréas.


## Networks

Ecriture du modèle DenseNet avec les différentes parties

## Results thorax

Resultats des entraînement pour les données du thorax avec plusieurs fonction de coût. Au format pth. 

## Results thorax_pancreas

Resultats des entraînement pour les données du thorax et pancreas. Au format pth. 

## Utils Wasserstein 

Distance de wasserstein codé ici + calcul psnr 

## Wgan 

Tentative d'écriture d'un modèle Wgan mais ne converge pas 

##  analyse_image_comparaison

Fichier utilisé pour sortir les images et comparer les différents modèles

## calcul_psnr_wasserstein

Fichier qui calcule le PSNR et MSE entre les sorties du modèle et la véritain terrain + sorties du modèles avec OT et vérité terrain.
Necessite image avec OT dans fichier Ecriture_test_wasserstein

## comparaison_modele

Fichier utilisé pour comparer le PSNR de plusieurs modèles

## ecriture_test_wasserstein

Fichier pour faire OT

## inf_denoising_DenseNet

Inférence sur les echantillons de test après entraînement du modèle. 

## proc_allpatients_pancreas

Fichier pour effectuer les différentes coupes pour les patients à partir des images 3D. Ici utilisé pour les données du pancréas 

## Test_autres_coupes

Inférence sur de nouvelles données (ici bassin et tête)

## train_denoising_DenseNet

Fichier pour entraîner DenseNet
Différentes fonctions de coût 

## Train-denoising_Gan

Fichier pour entraîner le WGAN 
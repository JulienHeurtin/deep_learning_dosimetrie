import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from collections.abc import Callable, Sequence
from torch.autograd import Variable
import numpy as np
import sys
sys.path.append('C:/Users/julien.heurtin/Documents/Stage_2A/deep_learning_dosimetrie')
from networks.denseNet import DenseNet
from wgan.critic import Critic
from torch.nn import MSELoss
from monai.data import Dataset, decollate_batch
from monai.inferers import sliding_window_inference
from monai.metrics import MAEMetric, MSEMetric
from monai.utils.type_conversion import convert_to_numpy
from monai.data.utils import list_data_collate
from monai.transforms import (
    Activations,
    AddChanneld,
    AsDiscrete,
    Compose,
    LoadImaged,
    RandFlipd,
    Flipd,
    RandRotate90,
    RandSpatialCrop,
    ScaleIntensityd,
    ConcatItemsd,
    MapTransform,
    ToTensord,
    EnsureType,
    EnsureTyped,
)
import os
import random
from glob import glob
from torch.autograd import grad as torch_grad


class Gan: 
    
    def __init__(
        self,
        spatial_dims: int =2,
        in_channels: int =2,
        out_channels: int=1,
        init_features: int = 64,
        growth_rate: int = 16,
        block_config: Sequence[int] = (4, 8, 16, 8),
        bn_size: int = 4,
        act: str | tuple = ("relu", {"inplace": True}),
        norm: str | tuple = "batch",
        dropout_prob: float = 0.0
    ) -> None:
        
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.input_size = 100  # Dimension de l'espace latent
        self.output_size = 784 # Taille de l'image générée (28x28 pour MNIST)
        self.num_epochs = 400
        self.lr = 1e-4
        self.critic_iterations = 5  # Nombre d'itérations du critique par mise à jour du générateur
        self.clamp_value = 0.01  # Valeur de la contrainte de pénalité Lipschitz pour WGAN
        
        self.generator = DenseNet(spatial_dims=spatial_dims,
                                  in_channels=in_channels,
                                  out_channels=out_channels,
                                  init_features = init_features,
                                  growth_rate=growth_rate,
                                  block_config=block_config,
                                  bn_size=bn_size,
                                  act=act,
                                  norm=norm,
                                  dropout_prob=dropout_prob
                                  ).to(self.device)
        
        self.critic = DenseNet(spatial_dims=spatial_dims,
                                  in_channels=1,
                                  out_channels=out_channels,
                                  init_features = init_features,
                                  growth_rate=growth_rate,
                                  block_config=block_config,
                                  bn_size=bn_size,
                                  act=act,
                                  norm=norm,
                                  dropout_prob=dropout_prob
                                  ).to(self.device)
        
    def gradient_penalty(self, real_data, generated_data):
        batch_size = real_data.size()[0]

        # Calculate interpolation
        alpha = torch.rand(batch_size, 1, 1, 1).to(self.device)

        interpolated = alpha * real_data + (1 - alpha) * generated_data
        interpolated = Variable(interpolated, requires_grad=True).to(self.device)

        # Calculate probability of interpolated examples
        prob_interpolated = self.critic(interpolated)

        # Calculate gradients of probabilities with respect to examples
        gradients = torch_grad(outputs=prob_interpolated, inputs=interpolated,
                            grad_outputs=torch.ones(prob_interpolated.size()).to(self.device),
                            create_graph=True, retain_graph=True)[0]

        # Gradients have shape (batch_size, num_channels, img_width, img_height),
        # so flatten to easily take norm per example in batch
        gradients = gradients.view(batch_size, -1)

        # Derivatives of the gradient close to 0 can cause problems because of
        # the square root, so manually calculate norm and add epsilon
        gradients_norm = torch.sqrt(torch.sum(gradients ** 2, dim=1) + 1e-12).to(self.device)

        # Return gradient penalty
        return ((gradients_norm - 1) ** 2).mean()
        
    def train(self):
        
        val_interval = 5
        pathDir = 'C:/Users/julien.heurtin/Documents/Stage_2A/Data_Medical/MCsimulations'
        trainFilenames = list()
        valFilenames = list()

        dirNames = sorted(glob(os.path.join(pathDir, 'sample_*')))
        random.seed(10)
        random.shuffle(dirNames)

        N = len(dirNames)
        nTest = int(0.1*N)
        nTrain = int(0.7*N)

        for dirName in dirNames[:nTrain]:
            edepLowFilename = os.path.join(dirName, 'low_edep.mhd')
            edepHighFilename = os.path.join(dirName, 'high_edep.mhd')
            ctFilename = os.path.join(dirName, 'ct.mhd') 
            
            tmpDict = {'dose': edepLowFilename, 'lbl': edepHighFilename, 'ct': ctFilename}
            
            trainFilenames.append(tmpDict)

        for dirName in dirNames[nTrain: N - nTest]:
            edepLowFilename = os.path.join(dirName, 'low_edep.mhd')
            edepHighFilename = os.path.join(dirName, 'high_edep.mhd')
            ctFilename = os.path.join(dirName, 'ct.mhd') 
            
            tmpDict = {'dose': edepLowFilename, 'lbl': edepHighFilename, 'ct': ctFilename}
            
            valFilenames.append(tmpDict)

        train_transforms = Compose(
            [
                LoadImaged(keys=['dose', 'lbl', 'ct'], image_only=True),
                # ScaleIntensityd(keys=['img', 'lbl'], minv=0.0, maxv=1.0),
                ToTensord(keys=['dose', 'lbl', 'ct']),
                AddChanneld(keys=['dose', 'lbl', 'ct']),
                ConcatItemsd(keys=['dose', 'ct'], name='img'),
                # RandFlipd(keys=['img', 'lbl'], prob=0.5, spatial_axis=2),
                # Flipd(keys=['img'], spatial_axis=2),
                EnsureTyped(keys=['img', 'lbl'])
            ]
        )

        train_ds = Dataset(data=trainFilenames, transform=train_transforms)
        train_loader = DataLoader(
            train_ds,
            batch_size=8,
            shuffle=True,
            num_workers=1,
            collate_fn=list_data_collate,
            pin_memory=torch.cuda.is_available(),
        )

        # create a validation data loader
        val_ds = Dataset(data=valFilenames, transform=train_transforms)
        val_loader = DataLoader(
            val_ds, 
            batch_size=1, 
            num_workers=1, 
            collate_fn=list_data_collate,
            pin_memory=torch.cuda.is_available(),
        )

        # Définir les optimiseurs
        generator_optimizer = optim.Adam(self.generator.parameters(), 1e-4)
        critic_optimizer = optim.Adam(self.critic.parameters(), 1e-4)
        
        # Boucle d'entraînement
        for epoch in range(self.num_epochs):
            
            self.generator.train()
            self.critic.train()
            
            for batch_data in train_loader:
                
                inputs, labels = batch_data['img'].to(self.device), batch_data['lbl'].to(self.device)

                #for _ in range(self.critic_iterations):
                # Entraînement du critique (critic)
                critic_optimizer.zero_grad()
                fake_images = self.generator(inputs)
                critic_real_output = self.critic(labels)
                critic_fake_output = self.critic(fake_images.detach())
                critic_loss = torch.mean(critic_fake_output) - torch.mean(critic_real_output) + Gan().gradient_penalty(labels, fake_images)
                critic_loss.backward()
                critic_optimizer.step()

                # Entraînement du générateur (generator)
                for p in self.generator.parameters():
                    p.requires_grad = False 
                generator_optimizer.zero_grad()
                outputs = self.generator(inputs)
                critic_fake_output = self.critic(outputs)
                generator_loss = -torch.mean(critic_fake_output)
                #generator_loss = generator_loss.clone().detach().requires_grad_(True)
                generator_loss.backward()
                generator_optimizer.step()

                # Affichage des informations d'entraînement
            if (epoch + 1) % val_interval == 0:
                    print(f"Critic Loss: {critic_loss.item():.4f}, Generator Loss: {generator_loss.item():.4f}")
                    with open('./big_results/GAN/generator_loss.npy', 'wb') as f:
                        np.save(f, generator_loss.item())
                    with open('./big_results/GAN/critic_loss.npy', 'wb') as f:
                        np.save(f, critic_loss.item())
            print("Epoque numéro:", epoch, "Perte du générateur:",generator_loss.item(),"Perte du critic:",critic_loss.item())

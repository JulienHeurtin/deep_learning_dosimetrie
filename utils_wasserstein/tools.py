import torch

import numpy as np


from utils_wasserstein.utils import im2vec, vec2im
from utils_wasserstein.ot import sliced_wasserstein


dtype  = torch.float64
device = torch.device('cpu')


def approach_by_wasserstein(source,cible, num_epoch=1000, lr=10):
	tens_source = torch.tensor(np.copy(im2vec(source)), dtype=dtype, device=device).requires_grad_()
	tens_target = torch.tensor(im2vec(cible), dtype=dtype, device=device)
	optimizer = torch.optim.SGD([tens_source], lr=lr)
	losses=[]

	reg = torch.nn.MSELoss()

	for epoch in range(num_epoch):
		loss = sliced_wasserstein(tens_source, tens_target, 2., dtype=dtype, device=device)
		#+ 1. * reg(tens_source, 0 * tens_target)
		losses.append(loss.clone().detach().cpu().numpy())
		loss.backward() 
		optimizer.step()
		optimizer.zero_grad()
		#print("Iter {} - loss {}".format(epoch, loss.clone().detach().item()))
	Xs_new = tens_source.clone().detach().cpu().numpy()
	return vec2im(Xs_new, source.shape)

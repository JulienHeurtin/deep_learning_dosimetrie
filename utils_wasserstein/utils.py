import torch

import numpy as np
import math
# import matplotlib.pylab as pl
# from matplotlib.pyplot import imread
# from mpl_toolkits.mplot3d import Axes3D
# import sklearn.cluster as skcluster
# import cv2


myPreferredFigsize=(8,8)

def psnr(im1,im2,vmax=None):
    """ 
           psnr - compute the Peack Signal to Noise Ratio

           p = psnr(x,y,vmax);

           defined by :
                  p = 10*log10( vmax^2 / |x-y|^2 )
              |x-y|^2 = mean( (x(:)-y(:)).^2 )
              if vmax is ommited, then
                 vmax = max(max(x(:)),max(y(:)))

    """

    if vmax==None :
        m1 = np.max( abs(im1) )
        m2 = np.max( abs(im2) )
        vmax = max(m1,m2)

    d = np.mean( ( im1 - im2 )**2 )

    return 10*math.log10( vmax**2/d )
    
def im2vec(I):
    """Converts and image to matrix (one pixel per line)"""
    return I.reshape((1, I.shape[0] * I.shape[1]))

def vec2im(I, shape):
    """Converts and image to matrix (one pixel per line)"""
    return I.reshape(shape)

# def mat2im(X, shape):
#     """Converts back a matrix to an image"""
#     return X.reshape(shape)


# def showImage(I):
#     pl.figure(figsize=myPreferredFigsize)
#     pl.imshow(I)
#     pl.axis('off')
#     pl.tight_layout()
#     pl.show()

# def showImageAsPointCloud(X, ax):
#     ax.set_xlim(0,1)
#     ax.scatter(X[:,0], X[:,1], X[:,2], c=X, marker='o', alpha=1.0)
#     ax.set_xlabel('R',fontsize=22)
#     ax.set_xticklabels([])
#     ax.set_ylim(0,1)
#     ax.set_ylabel('G',fontsize=22)
#     ax.set_yticklabels([])
#     ax.set_zlim(0,1)
#     ax.set_zlabel('B',fontsize=22)
#     ax.set_zticklabels([])
#     ax.grid('off')


# def im2colorcube(I,s):
#     """Converts and image to color cube of size s^3 (3D color histogram)"""
#     return I.reshape((I.shape[0] * I.shape[1], I.shape[2]))



